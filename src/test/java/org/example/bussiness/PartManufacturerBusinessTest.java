package org.example.bussiness;

import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.example.model.Company;
import org.example.model.Manufacturer;
import org.example.model.Part;
import org.example.model.PartManufacturer;
import org.example.repository.ManufacturerRepository;
import org.example.repository.PartRepository;
import org.example.repository.PartManufacturerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PartManufacturerBusinessTest {

  PartRepository partRepository;
  ManufacturerRepository manufacturerRepository;
  PartManufacturerRepository partManufacturerRepository;

  PartManufacturerBusiness partManufacturerBusiness;

  Part part;

  Manufacturer manufacturer;
  Company company;
  List<Manufacturer> manufacturerList;
  List<PartManufacturer> partManufacturerList;

  @Before
  public void setup() {
    partRepository = Mockito.mock(PartRepository.class);
    manufacturerRepository = Mockito.mock(ManufacturerRepository.class);
    partManufacturerRepository = Mockito.mock(PartManufacturerRepository.class);
    partManufacturerBusiness = new PartManufacturerBusiness(partRepository, manufacturerRepository,
        partManufacturerRepository);

    part = new Part(1L, "Part 1", null);

    company = new Company(1L, 200.0, "Nokia", null);
    manufacturer = new Manufacturer(1L, "Manufacturer 1", null);

    manufacturerList = Arrays.asList(
        new Manufacturer(1L, "Manufacturer 1", null),
        new Manufacturer(2L, "Manufacturer 2", null)
    );

    partManufacturerList = Arrays.asList(
        new PartManufacturer(1L, part, manufacturerList.get(0), 10, 20f),
        new PartManufacturer(2L, part, manufacturerList.get(1), 6, 10f)
    );
  }

  @Test
  public void testGetPartManufacturer_When_ManufacturerExits_Return_PartManufacturer() {
    String manufacturerName = "Manufacturer 1";
    Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

    PartManufacturer manufacturer1 = new PartManufacturer(null, part,
        manufacturer.get(), partManufacturerList.get(0).getQuantity(), partManufacturerList.get(0).getPrice());
    Mockito.when(partManufacturerRepository.getByPartAndManufacturer(any(), any()))
        .thenReturn(Optional.of(manufacturer1));

    Assert.assertEquals(partManufacturerBusiness.getPartManufacturer(partManufacturerList.get(0)), manufacturer1);
  }

  @Test
  public void testGetPartManufacturer_When_PartManufacturerDoesNotExits_Return_Empty() {
    String manufacturerName = "Manufacturer 1";
    Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

    PartManufacturer manufacturer1 = new PartManufacturer(null, part,
        manufacturer.get(), partManufacturerList.get(0).getQuantity(), partManufacturerList.get(0).getPrice());

    Mockito.when(partManufacturerRepository.getByPartAndManufacturer(any(), any())).thenReturn(Optional.empty());

    PartManufacturer actualPartManufacturer = partManufacturerBusiness.getPartManufacturer(manufacturer1);

    Assert.assertEquals(null, manufacturer1.getId());

  }

  @Test
  public void testGetPart_When_Part_Exists_Return_Part() {
    String partName = "Part 1";

    Mockito.when(partRepository.getByName(any()))
        .thenReturn(Optional.of(part));

    Optional<Part> actualPart = partManufacturerBusiness.getPart(partName);

    Assert.assertEquals("Part 1", actualPart.get().getName());
  }

  @Test
  public void testGetPart_When_PartNameNotFound_ReturnEmpty() {
    String partName = "Part 10";
    Mockito.when(partRepository.getByName(any()))
        .thenThrow(RuntimeException.class);

    Optional<Part> actualPart = partManufacturerBusiness.getPart(partName);

    Assert.assertEquals(Optional.empty(), actualPart);
  }

  @Test
  public void testGetPartManufacturerList_When_PartExistsButManufacturerNameDoesNotExists_Return_PartManufacturerList() {
    String partName = "part 1";
    String manufacturerName = "";

    Mockito.when(partRepository.getByName(any())).thenReturn(Optional.ofNullable(part));
    Mockito.when(manufacturerRepository.getByName(any())).thenReturn(Optional.empty());
    Mockito.when(partManufacturerRepository.listQuantity(any(), any()))
        .thenReturn(partManufacturerList);

    List<PartManufacturer> actualPartManufacturerList = partManufacturerBusiness.getPartManufacturerList(
        partName, manufacturerName);

    Assert.assertEquals(partManufacturerList.size(), actualPartManufacturerList.size());
    Assert.assertEquals(partManufacturerList, actualPartManufacturerList);
  }

  @Test
  public void testGetPartManufacturerList_When_PartDoesNotExists_Return_EmptyList() {

    Optional<Manufacturer> manufacturer = Optional.of(manufacturerList.get(0));

    Mockito.when(partRepository.getByName(any())).thenReturn(Optional.empty());
    Mockito.when(manufacturerRepository.getByName(any())).thenReturn(manufacturer);

    List<PartManufacturer> expectedPartManufacturerList = new ArrayList<>();

    Assert.assertEquals(expectedPartManufacturerList.size(), 0);
  }

  @Test
  public void testGetPartManufacturerWithUpdatedQuantiity_When_PartManufacerListContainsBoughtPartManufacturer_Then_Return_UpdatedPartManufacturer() {
    PartManufacturer boughtPartManufacturer =
        new PartManufacturer(1L, part, manufacturer, 7, 15f);

    PartManufacturer expectedPartManufacturerWithQuantity =
        new PartManufacturer(1L, part, manufacturer, 3, 15f);

    PartManufacturer actualPartManufacturerWithQuantity =
        partManufacturerBusiness.getPartManufacturerWithUpdatedQuantiity(partManufacturerList,
            boughtPartManufacturer);

    Assert.assertNotNull(actualPartManufacturerWithQuantity);
    Assert.assertEquals(expectedPartManufacturerWithQuantity.getId(),
        actualPartManufacturerWithQuantity.getId());
  }

  @Test
  public void testGetManufacturer_When_Manufacturer_Exists_Return_Manufacturer() {
    String manufacturerName = "Manufacturer 1";

    Mockito.when(manufacturerRepository.getByName(manufacturerName))
        .thenReturn(Optional.of(manufacturer));

    Optional<Manufacturer> excpectedManufacturer = Optional.of(manufacturer);
    Optional<Manufacturer> actualManufacturer = partManufacturerBusiness.getManufacturer(
        manufacturerName);

    Assert.assertEquals(excpectedManufacturer.get().getId(), actualManufacturer.get().getId());
    Assert.assertEquals(excpectedManufacturer.get().getName(), actualManufacturer.get().getName());
  }

  @Test
  public void testGetPartManufacturer_When_PartManufacturer_Exists_ReturnPartManufacturer() {
    String partName = "Part 1";
    String manufacturerName = "Manufacturer 1";

    Mockito.when(partManufacturerRepository.getByPartAndManufacturer(partName, manufacturerName))
        .thenReturn(Optional.of(partManufacturerList.get(0)));

    Optional<PartManufacturer> expectedPartManufacturer = Optional.of(partManufacturerList.get(0));
    Optional<PartManufacturer> actualPartManufacturer = partManufacturerBusiness.getPartManufacturer(
        partName, manufacturerName);

    Assert.assertEquals(expectedPartManufacturer.get().getId(),
        actualPartManufacturer.get().getId());
    Assert.assertEquals(expectedPartManufacturer.get().getPart(),
        actualPartManufacturer.get().getPart());
  }

  @Test
  public void testGetPartManufacturer_When_RunTimeException_Thrown_Return_Empty_PartManufacturer() {
    String partName = "Part 1";
    String manufacturerName = "Manufacturer 1";

    Mockito.when(partManufacturerRepository.getByPartAndManufacturer(partName, manufacturerName))
        .thenThrow(RuntimeException.class);

    Optional<PartManufacturer> actualPartManufacturer = partManufacturerBusiness.getPartManufacturer(
        partName, manufacturerName);

    Assert.assertEquals(Optional.empty(), actualPartManufacturer);
  }

  @Test
  public void testProcessPartManufacturerQuantity_When_FetchedPartManufacturer_Does_Not_Exists_Return_PartManufacturer() {
    String partName = "Part 1";
    String manufacturerName = "Manufacturer 1";
    int quantity = 10;
    float price = 5.5f;

    Optional<Part> part = Optional.of(new Part(1L, partName, null));
    Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

    PartManufacturer inputPartManufacturer = new PartManufacturer(null, part.get(),
        manufacturer.get(), quantity, price);
    Optional<PartManufacturer> fetchedPartManufacturer = Optional.empty();

    PartManufacturer actualPartManufacturer = partManufacturerBusiness.processPartManufacturerQuantity(
        fetchedPartManufacturer, inputPartManufacturer);

    Assert.assertEquals(inputPartManufacturer.getPart(), actualPartManufacturer.getPart());
    Assert.assertEquals(inputPartManufacturer.getManufacturer(),
        actualPartManufacturer.getManufacturer());
    Assert.assertEquals(inputPartManufacturer.getQuantity(), actualPartManufacturer.getQuantity());
    Assert.assertEquals(inputPartManufacturer.getPrice(), actualPartManufacturer.getPrice(), 0.0);
  }

  @Test
  public void testGetPartManufacturerList_When_PartAndManufacturerExists_Then_ReturnPartManufacturerList() {
    String partName = "Part 1";
    String manufacturerName = "Manufacturer 1";
    float price = 18.5f;
    int quantity = 4;

    Optional<Part> part = Optional.of(new Part(1L, partName, null));
    Optional<Manufacturer> manufacturer = Optional.of(new Manufacturer(1L, manufacturerName, null));

    List<PartManufacturer> partManufacturerList = List.of(
        new PartManufacturer(1L, part.get(), manufacturer.get(), quantity, price)
    );

    Mockito.when(partManufacturerRepository.listQuantity(partName, manufacturerName))
        .thenReturn(partManufacturerList);

    Mockito.when(partRepository.getByName(partName)).thenReturn(part);
    Mockito.when(manufacturerRepository.getByName(manufacturerName)).thenReturn(manufacturer);

    List<PartManufacturer> actualPartManufacturerList = partManufacturerBusiness.getPartManufacturerList(
        partName, manufacturerName);

    Assert.assertEquals(partManufacturerList.size(), actualPartManufacturerList.size());
    Assert.assertEquals(partManufacturerList, actualPartManufacturerList);
  }
}
