package org.example.bussiness;

import static org.mockito.ArgumentMatchers.any;

import java.util.Optional;
import org.example.model.Part;
import org.example.repository.PartRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PartBusinessTest {

  PartRepository partRepository;
  PartBusiness partBusiness;

  @Before
  public void setup() {
    partRepository = Mockito.mock(PartRepository.class);
    partBusiness = new PartBusiness(partRepository);
  }

  @Test
  public void testGetPart_When_PartExists_Then_ReturnPart() {
    String partName = "part-1";

    Part part = new Part(1L, partName, null);

    Mockito.when(partRepository.getByName(partName))
        .thenReturn(Optional.of(part));

    Part actualPart = partBusiness.getPart(partName);

    Assert.assertEquals(part, actualPart);
  }

  @Test
  public void testGetPart_When_PartDoesNotExists_Then_ReturnEmpty() {
    Part part = new Part(null, null, null);

    Mockito.when(partRepository.getByName(any()))
        .thenReturn(Optional.empty());

    Part actualPart = partBusiness.getPart(any());

    Assert.assertEquals(part, actualPart);
  }
}
