package org.example.menu.subMenu;

import java.util.Arrays;
import java.util.List;
import org.example.constants.InputConstants;
import org.example.constants.Message;
import org.example.menu.MainMenu;
import org.example.menu.Menu;
import org.example.menu.operationMenu.AddManufacturerMenu;
import org.example.menu.operationMenu.AddPartMenu;
import org.example.menu.operationMenu.RemoveManufacturerMenu;

public class DataMenu extends Menu {

  private final List<String> subMenus;

  public DataMenu() {
    this.subMenus = Arrays.asList(InputConstants.ADD_PART, InputConstants.ADD_MANUFACTURER,
        InputConstants.REMOVE_MANUFACTURER, InputConstants.BACK);
  }

  @Override
  public void display() {
    System.out.println("\nData Menu\n");
    for (int i = 0; i < 4; i++) {
      System.out.println((i + 1) + ": " + subMenus.get(i));
    }
  }

  @Override
  public void handleUserChoice(int choice) {
    if (choice < 1 || choice > 4) {
      System.err.println(Message.INVALID_INPUT_BOUND);
      return;
    }

    switch (choice) {
      case 1:
        // call add menu
        AddPartMenu addPartMenu = new AddPartMenu();
        addPartMenu.handleMenuItem();
        break;
      case 2:
        // call add manufacture
        AddManufacturerMenu addManufacturerMenu = new AddManufacturerMenu();
        addManufacturerMenu.handleMenuItem();
        break;
      case 3:
        // call remove manufacture
        RemoveManufacturerMenu removeManufacturerMenu = new RemoveManufacturerMenu();
        removeManufacturerMenu.handleMenuItem();
        break;
      case 4:
        MainMenu mainMenu = new MainMenu();
        int nextChoice = 0;
        do {
          mainMenu.display();
          nextChoice = mainMenu.getUserChoice();
          mainMenu.handleUserChoice(nextChoice);
        } while (nextChoice != 4);
        break;
    }

  }
}
