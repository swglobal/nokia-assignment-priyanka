package org.example.menu.subMenu;

import java.util.Arrays;
import java.util.List;
import org.example.constants.InputConstants;
import org.example.constants.Message;
import org.example.menu.MainMenu;
import org.example.menu.Menu;
import org.example.menu.operationMenu.AddQuantityMenu;
import org.example.menu.operationMenu.ListQuantityMenu;

public class ManufacturersMenu extends Menu {

  private final List<String> subMenus;

  public ManufacturersMenu() {
    this.subMenus = Arrays.asList(InputConstants.ADD_QUANTITY, InputConstants.LIST_QUANTITY,
        InputConstants.BACK);
  }

  @Override
  public void display() {
    System.out.println(Message.MANUFACTURES_MENU);
    for (int i = 0; i < subMenus.size(); i++) {
      System.out.println((i + 1) + ". " + subMenus.get(i));
    }
  }

  @Override
  public void handleUserChoice(int choice) {
    if (choice < 1 || choice > 4) {
      System.err.println(Message.INVALID_INPUT_BOUND);
      return;
    }
    switch (choice) {
      case 1:
        // call add quantity
        AddQuantityMenu addQuantityMenu = new AddQuantityMenu();
        addQuantityMenu.handleMenuItem();
        break;
      case 2:
        // call list quantity
        ListQuantityMenu listQuantityMenu = new ListQuantityMenu();
        listQuantityMenu.handleMenuItem();
        break;
      case 3:
        MainMenu mainMenu = new MainMenu();
        int nextChoice = 0;
        do {
          mainMenu.display();
          nextChoice = mainMenu.getUserChoice();
          mainMenu.handleUserChoice(nextChoice);
        } while (nextChoice != 4);
        break;
    }
  }

}
