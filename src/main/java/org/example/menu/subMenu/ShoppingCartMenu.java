package org.example.menu.subMenu;

import java.util.Arrays;
import java.util.List;
import org.example.constants.InputConstants;
import org.example.constants.Message;
import org.example.menu.MainMenu;
import org.example.menu.Menu;
import org.example.menu.operationMenu.AddMoneyMenu;
import org.example.menu.operationMenu.BuyPartsMenu;
import org.example.menu.operationMenu.ListPartsMenu;

public class ShoppingCartMenu extends Menu {

  private final List<String> subMenus;

  public ShoppingCartMenu() {
    this.subMenus = Arrays.asList(InputConstants.ADD_MONEY, InputConstants.BUY_PARTS,
        InputConstants.LIST_PARTS,
        InputConstants.BACK);
  }

  @Override
  public void display() {
    System.out.println(Message.SHOPPING_MENU);
    for (int i = 0; i < subMenus.size(); i++) {
      System.out.println((i + 1) + ". " + subMenus.get(i));
    }
  }

  @Override
  public void handleUserChoice(int choice) {
    if (choice < 1 || choice > 4) {
      System.err.println(Message.INVALID_INPUT_BOUND);
      return;
    }
    switch (choice) {
      case 1:
        // call add money
        AddMoneyMenu addMoneyMenu = new AddMoneyMenu();
        addMoneyMenu.handleMenuItem();
        return;
      case 2:
        // call buy parts
        BuyPartsMenu buyPartsMenu = new BuyPartsMenu();
        buyPartsMenu.handleMenuItem();
        return;
      case 3:
        // call list parts
        ListPartsMenu listPartsMenu = new ListPartsMenu();
        listPartsMenu.handleMenuItem();
        return;
      case 4:
        MainMenu mainMenu = new MainMenu();
        int nextChoice = 0;
        do {
          mainMenu.display();
          nextChoice = mainMenu.getUserChoice();
          mainMenu.handleUserChoice(nextChoice);
        } while (nextChoice != 4);
        break;
    }
  }

}
