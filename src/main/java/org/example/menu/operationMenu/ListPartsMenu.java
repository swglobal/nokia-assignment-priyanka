package org.example.menu.operationMenu;

import java.util.List;
import java.util.Scanner;
import org.example.model.ShoppingCart;
import org.example.service.ShoppingCartService;
import org.example.util.ServiceFactory;

public class ListPartsMenu {

  final Scanner scanner = new Scanner(System.in);

  private final ShoppingCartService shoppingCartService;

  public ListPartsMenu() {
    this.shoppingCartService = ServiceFactory.getCompanyStockService();
  }

  public void handleMenuItem() {
    List<ShoppingCart> shoppingCartList = shoppingCartService.listParts();

    if (shoppingCartList.size() > 0) {
      displayCompanyStocks(shoppingCartList);
    }
  }

  private void displayCompanyStocks(List<ShoppingCart> shoppingCartList) {
    System.out.println();
    for (ShoppingCart shoppingCart : shoppingCartList) {
      System.out.println(String.format("Part : %1$s, manufacturer: %2$s, quantity: %3$d",
          shoppingCart.getPart().getName(), shoppingCart.getManufacturer().getName(),
          shoppingCart.getQuantity()));
    }
  }
}
