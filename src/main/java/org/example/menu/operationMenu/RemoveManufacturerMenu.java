package org.example.menu.operationMenu;

import java.util.Scanner;
import org.example.constants.Message;
import org.example.service.ManufacturerService;
import org.example.util.ServiceFactory;

public class RemoveManufacturerMenu {

  final Scanner scanner = new Scanner(System.in);

  private final ManufacturerService manufacturerService;

  public RemoveManufacturerMenu() {
    this.manufacturerService = ServiceFactory.getManufacturerService();
  }

  public void handleMenuItem() {
    System.out.print(Message.MANUFACTURER_NAME);
    String manufacturerName = scanner.nextLine();
    manufacturerService.removeManufacturer(manufacturerName);
  }
}
