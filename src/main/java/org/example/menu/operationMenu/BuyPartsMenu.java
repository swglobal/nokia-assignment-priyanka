package org.example.menu.operationMenu;

import java.util.Scanner;
import org.example.constants.Message;
import org.example.service.ShoppingCartService;
import org.example.util.ServiceFactory;

public class BuyPartsMenu {

  final Scanner scanner = new Scanner(System.in);

  private final ShoppingCartService shoppingCartService;

  public BuyPartsMenu() {
    this.shoppingCartService = ServiceFactory.getCompanyStockService();
  }

  public void handleMenuItem() {
    System.out.println(Message.PART_NAME);
    String partName = scanner.nextLine();
    System.out.println(Message.QUANTITY);
    int quantity = scanner.nextInt();

    shoppingCartService.buyParts(partName, quantity);
  }
}
