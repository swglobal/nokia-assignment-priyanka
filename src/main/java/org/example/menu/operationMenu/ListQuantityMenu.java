package org.example.menu.operationMenu;

import java.util.List;
import java.util.Scanner;
import org.example.constants.Message;
import org.example.model.PartManufacturer;
import org.example.service.PartManufacturerService;
import org.example.util.ServiceFactory;

public class ListQuantityMenu {

  final Scanner scanner = new Scanner(System.in);

  private final PartManufacturerService partManufacturerService;

  public ListQuantityMenu() {
    this.partManufacturerService = ServiceFactory.getPartManufacturerService();
  }

  public void handleMenuItem() {
    System.out.println(Message.PART_NAME);
    String partName = scanner.nextLine();
    System.out.println(Message.MANUFACTURER_NAME);
    String manufacturerName = scanner.nextLine();

    List<PartManufacturer> partManufacturerList = partManufacturerService.listQuantity(partName,
        manufacturerName);

    if (partManufacturerList.size() > 0) {
      displayPartManufacturers(partManufacturerList);
    }
  }

  private void displayPartManufacturers(List<PartManufacturer> partManufacturerList) {
    for (PartManufacturer partManufacturer : partManufacturerList) {
      System.out.println("\nPart name: " + partManufacturer.getPart().getName());
      System.out.println("Manufacturer name: " + partManufacturer.getManufacturer().getName());
      System.out.println(String.format("Price: %1$f, Quantity: %2$d", partManufacturer.getPrice(),
          partManufacturer.getQuantity()));
    }
    System.out.println();
  }

}
