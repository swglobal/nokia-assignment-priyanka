package org.example.menu.operationMenu;

import java.util.Scanner;
import org.example.constants.Message;
import org.example.service.CompanyService;
import org.example.util.ServiceFactory;

public class AddMoneyMenu {

  final Scanner scanner = new Scanner(System.in);

  private final CompanyService companyService;

  public AddMoneyMenu() {
    this.companyService = ServiceFactory.getCompanyService();
  }

  public void handleMenuItem() {
    System.out.println(Message.AMOUNT);
    Double amount = scanner.nextDouble();
    companyService.addMoney(amount);
  }
}
