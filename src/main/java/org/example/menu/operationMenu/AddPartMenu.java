package org.example.menu.operationMenu;

import java.util.Scanner;
import org.example.constants.Message;
import org.example.service.PartService;
import org.example.util.ServiceFactory;

public class AddPartMenu {

    final Scanner scanner = new Scanner(System.in);

    private final PartService partService;

    public AddPartMenu() {
        this.partService = ServiceFactory.getPartService();
    }

    public void handleMenuItem() {
        System.out.print(Message.PART_NAME);
        String partName = scanner.nextLine();
        partService.addPart(partName);
    }

}
