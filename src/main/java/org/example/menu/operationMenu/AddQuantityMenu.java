package org.example.menu.operationMenu;

import java.util.Scanner;
import org.example.constants.Message;
import org.example.service.PartManufacturerService;
import org.example.util.ServiceFactory;

public class AddQuantityMenu {

  final Scanner scanner = new Scanner(System.in);

  private final PartManufacturerService partManufacturerService;

  public AddQuantityMenu() {
    this.partManufacturerService = ServiceFactory.getPartManufacturerService();
  }

  public void handleMenuItem() {
    System.out.println(Message.PART_NAME);
    String partName = scanner.nextLine();
    System.out.println(Message.MANUFACTURER_NAME);
    String manufacturerName = scanner.nextLine();
    System.out.println(Message.QUANTITY);
    int quantity = scanner.nextInt();
    System.out.println(Message.PRICE);
    float price = scanner.nextFloat();

    partManufacturerService.addQuantity(partName, manufacturerName, quantity, price);
  }
}
