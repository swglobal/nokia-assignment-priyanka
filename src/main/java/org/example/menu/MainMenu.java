package org.example.menu;

import java.util.Arrays;
import java.util.List;
import org.example.constants.InputConstants;
import org.example.constants.Message;

public class MainMenu extends Menu {

  private final List<String> subMenus;

  public MainMenu() {
    this.subMenus = Arrays.asList(InputConstants.DATA, InputConstants.MANUFACTURER,
        InputConstants.CART, InputConstants.EXIT);
  }

  @Override
  public void display() {
    System.out.println(Message.MAIN_MENU);
    for (int i = 0; i < 4; i++) {
      System.out.println((i + 1) + ": " + subMenus.get(i));
    }
  }

  @Override
  public void handleUserChoice(int choice) {

    if (choice == 4) {
      System.exit(0);
      System.out.println(Message.EXIT_MESSAGE);
      return;
    }

    if (choice < 1 || choice > 4) {
      System.err.println(Message.INVALID_INPUT_BOUND);
      return;
    }

    SubMenu subMenu = new SubMenu(choice);
    subMenu.handleChoice();
  }

}