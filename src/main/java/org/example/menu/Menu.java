package org.example.menu;

import java.util.InputMismatchException;
import java.util.Scanner;
import org.example.constants.Message;

public abstract class Menu {

  protected final Scanner scanner = new Scanner(System.in);

  public abstract void display();

  public abstract void handleUserChoice(int choice);

  public int getUserChoice() {
    int choice;
    do {
      choice = 0;
      System.out.print(Message.ENTER_CHOICE);
      try {
        choice = Integer.parseInt(String.valueOf(scanner.nextInt()));
      } catch (InputMismatchException e) {
        System.out.println(Message.INVALID_INPUT);
        scanner.next();
      }
    } while (choice == 0);

    return choice;
  }
}