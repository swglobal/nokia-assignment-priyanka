package org.example.menu;

import java.util.InputMismatchException;
import java.util.Scanner;
import org.example.constants.Message;
import org.example.menu.subMenu.ShoppingCartMenu;
import org.example.menu.subMenu.DataMenu;
import org.example.menu.subMenu.ManufacturersMenu;
import org.example.service.CompanyService;
import org.example.service.ShoppingCartService;
import org.example.service.ManufacturerService;
import org.example.service.PartManufacturerService;
import org.example.service.PartService;
import org.example.util.ServiceFactory;

public class SubMenu {

  private final Scanner scanner = new Scanner(System.in);
  int choice;
  private final PartService partService;
  private final ManufacturerService manufacturerService;
  private final PartManufacturerService partManufacturerService;
  private final CompanyService companyService;
  private final ShoppingCartService shoppingCartService;

  public SubMenu(int choice) {
    this.choice = choice;
    this.partService = ServiceFactory.getPartService();
    this.manufacturerService = ServiceFactory.getManufacturerService();
    this.partManufacturerService = ServiceFactory.getPartManufacturerService();
    this.companyService = ServiceFactory.getCompanyService();
    this.shoppingCartService = ServiceFactory.getCompanyStockService();
  }

  public void handleChoice(){
    int nextChoice = 0;

    switch (choice) {

      case 1:
        // call data menu
        while(nextChoice!= 4) {
          DataMenu dataMenu = new DataMenu();
          dataMenu.display();
          System.out.print(Message.ENTER_CHOICE);
            try {
              nextChoice = Integer.parseInt(String.valueOf(scanner.nextInt()));
              dataMenu.handleUserChoice(nextChoice);
            } catch (InputMismatchException e) {
              System.out.println(Message.INVALID_INPUT);
              scanner.next();
          }
        }
        break;

      case 2:
        // call Manufacturers menu
        while(nextChoice!= 3) {
          ManufacturersMenu manufacturersMenu = new ManufacturersMenu();
          manufacturersMenu.display();
          System.out.print(Message.ENTER_CHOICE);
          try {
            nextChoice = Integer.parseInt(String.valueOf(scanner.nextInt()));
            manufacturersMenu.handleUserChoice(nextChoice);
          } catch (InputMismatchException e) {
            System.out.println(Message.INVALID_INPUT);
            scanner.next();
          }
        }
        break;

      case 3:
        // call Shopping Cart menu
        while(nextChoice!= 4) {

          ShoppingCartMenu shoppingCartMenu = new ShoppingCartMenu();
          shoppingCartMenu.display();
          System.out.print(Message.ENTER_CHOICE);
          try {
            nextChoice = Integer.parseInt(String.valueOf(scanner.nextInt()));
            shoppingCartMenu.handleUserChoice(nextChoice);
          } catch (InputMismatchException e) {
            System.out.println(Message.INVALID_INPUT);
            scanner.next();
          }
        }
        break;

      case 4:
        // exit
        System.out.println(Message.EXIT_MESSAGE);
        System.exit(0);
        break;
    }
  }

}
