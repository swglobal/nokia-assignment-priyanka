package org.example.model;


import static javax.persistence.GenerationType.AUTO;

import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "companies")
public class Company {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private Double balance;

    private String name;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ShoppingCart> shoppingCartList;

    public Company() {
    }

    public Company(Long id, Double balance, String name, List<ShoppingCart> shoppingCartList) {
        this.id = id;
        this.balance = balance;
        this.name = name;
        this.shoppingCartList = shoppingCartList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShoppingCart> getCompanyStockList() {
        return shoppingCartList;
    }

    public void setCompanyStockList(List<ShoppingCart> shoppingCartList) {
        this.shoppingCartList = shoppingCartList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        Company company = (Company) o;
        return Objects.equals(getId(), company.getId()) && Objects.equals(getBalance(), company.getBalance()) && Objects.equals(getName(), company.getName()) && Objects.equals(getCompanyStockList(), company.getCompanyStockList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBalance(), getName(), getCompanyStockList());
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", balance=" + balance +
                ", name='" + name + '\'' +
                '}';
    }
}
