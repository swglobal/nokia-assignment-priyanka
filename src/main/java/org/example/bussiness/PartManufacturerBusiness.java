package org.example.bussiness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.example.model.Manufacturer;
import org.example.model.Part;
import org.example.model.PartManufacturer;
import org.example.repository.ManufacturerRepository;
import org.example.repository.PartRepository;
import org.example.repository.PartManufacturerRepository;

public class PartManufacturerBusiness {

  private final PartRepository partRepository;
  private final ManufacturerRepository manufacturerRepository;

  private final PartManufacturerRepository partManufacturerRepository;

  public PartManufacturerBusiness(PartRepository partRepository, ManufacturerRepository manufacturerRepository,
      PartManufacturerRepository partManufacturerRepository) {
    this.partRepository = partRepository;
    this.manufacturerRepository = manufacturerRepository;
    this.partManufacturerRepository = partManufacturerRepository;
  }

  public PartManufacturer getPartManufacturer(PartManufacturer inputPartManufacturer) {
    PartManufacturer newPartManufacturer = null;

    String partName = inputPartManufacturer.getPart().getName();
    String manufacturerName = inputPartManufacturer.getManufacturer().getName();

    try {
      Optional<PartManufacturer> fetchedPartManufacturer = getPartManufacturer(partName,
          manufacturerName);
      newPartManufacturer = processPartManufacturerQuantity(fetchedPartManufacturer,
          inputPartManufacturer);
      return newPartManufacturer;
    } catch (Exception e) {
      return newPartManufacturer;
    }
  }

  public PartManufacturer processPartManufacturerQuantity(
      Optional<PartManufacturer> fetchedPartManufacturer, PartManufacturer inputPartManufacturer) {
    if (fetchedPartManufacturer.isPresent()) {
      fetchedPartManufacturer.get().setQuantity(
          fetchedPartManufacturer.get().getQuantity() + inputPartManufacturer.getQuantity());
      fetchedPartManufacturer.get().setPrice(inputPartManufacturer.getPrice());
      return fetchedPartManufacturer.get();
    } else {
      return inputPartManufacturer;
    }
  }

  public List<PartManufacturer> getPartManufacturerList(String partName,
      String manufacturerName) {

    if (partName.isEmpty()) {
//            Formatter.printException("Part Name can't be empty ! Please enter Part Name");
      return new ArrayList<>();
    }

    Optional<Part> part = getPart(partName);
    Optional<Manufacturer> manufacturer = getManufacturer(manufacturerName);

    if (part.isEmpty()) {
      String message = String.format("Part with Name '%s' does not exists", partName);
//            Formatter.printException(message);
      return new ArrayList<>();
    }

    List<PartManufacturer> partManufacturerList;
    if (manufacturer.isPresent()) {
      partManufacturerList = partManufacturerRepository.listQuantity(partName, manufacturerName);
    } else {
      partManufacturerList = partManufacturerRepository.listQuantity(partName, "");
    }

    return partManufacturerList;
  }

  public PartManufacturer getPartManufacturerWithUpdatedQuantiity(
      List<PartManufacturer> partManufacturerList,
      PartManufacturer boughtPartManufacturer) {

    for (PartManufacturer partManufacturer : partManufacturerList) {
      if (partManufacturer.getId() == boughtPartManufacturer.getId()) {
        partManufacturer.setQuantity(
            partManufacturer.getQuantity() - boughtPartManufacturer.getQuantity());
        return partManufacturer;
      }
    }
    return null;
  }

  public Optional<Part> getPart(String partName) {
    try {
      return partRepository.getByName(partName);
    } catch (Exception ex) {
      String exceptionMessage = String.format("Part with Name '%s' does not exist", partName);
      System.err.println(exceptionMessage);
      return Optional.empty();
    }
  }

  public Optional<Manufacturer> getManufacturer(String manufacturerName) {
    try {
      return manufacturerRepository.getByName(manufacturerName);
    } catch (Exception ex) {
      String exceptionMessage = String.format("Manufacturer with Name '%s' does not exist",
          manufacturerName);
      System.err.println(exceptionMessage);
      return Optional.empty();
    }
  }

  public Optional<PartManufacturer> getPartManufacturer(String partName, String manufacturerName) {
    try {
      return partManufacturerRepository.getByPartAndManufacturer(partName, manufacturerName);
    } catch (Exception ex) {
      String exceptionMessage = String.format(
          "PartManufacturer with Part name '%1$s' and Manufacturer name '%2$s' does not exist",
          partName, manufacturerName);
      System.err.println(exceptionMessage);
      return Optional.empty();
    }
  }
}
