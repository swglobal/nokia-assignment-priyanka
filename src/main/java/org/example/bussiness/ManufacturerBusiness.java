package org.example.bussiness;

import java.util.Optional;
import org.example.model.Manufacturer;
import org.example.repository.ManufacturerRepository;

public class ManufacturerBusiness {

  private final ManufacturerRepository manufacturerRepository;

  public ManufacturerBusiness(ManufacturerRepository manufacturerRepository) {
    this.manufacturerRepository = manufacturerRepository;
  }


  public Manufacturer getManufacturer(String manufacturerName) {
    Optional<Manufacturer> manufacturer = manufacturerRepository.getByName(manufacturerName);

    Manufacturer newManufacturer = null;
    if (manufacturer.isEmpty()) {
      newManufacturer = new Manufacturer(null, manufacturerName, null);
    } else {
      newManufacturer = manufacturer.get();
    }
    return newManufacturer;
  }
}
