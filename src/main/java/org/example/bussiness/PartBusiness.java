package org.example.bussiness;

import java.util.Optional;
import org.example.model.Part;
import org.example.repository.PartRepository;

public class PartBusiness {

  private final PartRepository partRepository;

  public PartBusiness(PartRepository partRepository) {
    this.partRepository = partRepository;
  }

  public Part getPart(String partName) {
    Optional<Part> part = partRepository.getByName(partName);

    Part newPart = null;
    if (part.isEmpty()) {
      newPart = new Part(null, partName, null);
    } else {
      newPart = part.get();
    }

    return newPart;
  }

}
