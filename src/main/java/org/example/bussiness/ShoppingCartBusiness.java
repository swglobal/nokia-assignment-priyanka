package org.example.bussiness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.example.constants.Message;
import org.example.model.Company;
import org.example.model.ShoppingCart;
import org.example.model.PartManufacturer;
import org.example.repository.ShoppingCartRepository;

public class ShoppingCartBusiness {

  private final ShoppingCartRepository shoppingCartRepository;

  public ShoppingCartBusiness(ShoppingCartRepository shoppingCartRepository) {
    this.shoppingCartRepository = shoppingCartRepository;
  }

  public List<PartManufacturer> getBoughtPartManufacturers(
      List<PartManufacturer> partManufacturerList, Company company, int required_quantity) {
    List<PartManufacturer> boughtPartManufacturerList = new ArrayList<>();
    Double totalCost = 0.0;

    partManufacturerList.sort((a, b) ->
        Float.compare(a.getPrice(), b.getPrice())
    );

    for (PartManufacturer partManufacturer : partManufacturerList) {
      int partsToBuy = Math.min(partManufacturer.getQuantity(), required_quantity);

      boughtPartManufacturerList.add(
          new PartManufacturer(partManufacturer.getId(), partManufacturer.getPart(),
              partManufacturer.getManufacturer(), partsToBuy, partManufacturer.getPrice()));
      required_quantity -= partsToBuy;
      totalCost += partsToBuy * partManufacturer.getPrice();

      if (required_quantity == 0) {
        break;
      }
    }

    if (required_quantity > 0) {
      System.err.println(Message.PARTS_NOT_AVAILABLE);
      return new ArrayList<>();
    } else if (totalCost > company.getBalance()) {
      System.err.println(Message.MONEY_NOT_ENOUGH);
      return new ArrayList<>();
    }

    return boughtPartManufacturerList;
  }

  public ShoppingCart getCompanyStock(ShoppingCart inputShoppingCart) {
    Optional<ShoppingCart> fetchedCompanyStock = shoppingCartRepository.getByPartNameAndManufacturerName
        (inputShoppingCart.getPart().getName(), inputShoppingCart.getManufacturer().getName());
    if (fetchedCompanyStock.isPresent()) {
      fetchedCompanyStock.get()
          .setQuantity(fetchedCompanyStock.get().getQuantity() + inputShoppingCart.getQuantity());
      return fetchedCompanyStock.get();
    }
    return inputShoppingCart;
  }

}
