package org.example.repository;

import java.util.List;
import java.util.Optional;
import org.example.model.Part;
import org.example.util.DBSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class PartRepository {

  private final SessionFactory sessionFactory = DBSession.buildSession(Part.class);

  public void savePart(Part part) {
    Session session = sessionFactory.openSession();
    Transaction transaction = session.beginTransaction();

    session.persist(part);
    transaction.commit();

    session.close();
  }

  public Optional<Part> getByName(String partName) {
    Session session = sessionFactory.openSession();

    Query<Part> query = session.createQuery("SELECT p FROM Part p WHERE p.name = :partName",
        Part.class);
    query.setParameter("partName", partName);
    List<Part> parts = query.list();

    session.close();

    if (parts.size() > 0) {
      return Optional.of(parts.get(0));
    } else {
      return Optional.empty();
    }
  }

  public List<Part> getAll() {
    Session session = sessionFactory.openSession();
    List<Part> partList = session.createQuery("FROM Part", Part.class).list();

    session.close();
    return partList;
  }

}
