package org.example.repository;

import java.util.List;
import java.util.Optional;
import org.example.model.Company;
import org.example.model.ShoppingCart;
import org.example.util.DBSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class ShoppingCartRepository {

  private final SessionFactory sessionFactory = DBSession.getSession(ShoppingCart.class);

  public Optional<ShoppingCart> getByPartNameAndManufacturerName(String partName,
      String manufacturerName) {
    Session session = sessionFactory.openSession();
    String hql = "FROM ShoppingCart cs where part.name=:partName AND manufacturer.name=:manufacturerName";
    Query<ShoppingCart> query = session.createQuery(hql, ShoppingCart.class);
    query.setParameter("partName", partName);
    query.setParameter("manufacturerName", manufacturerName);

    List<ShoppingCart> shoppingCartList = query.list();
    session.close();

    if (shoppingCartList.size() > 0) {
      return Optional.of(shoppingCartList.get(0));
    } else {
      return Optional.empty();
    }
  }

  public void saveCompanyStock(ShoppingCart shoppingCart) {
    Session session = sessionFactory.openSession();
    Transaction transaction = session.beginTransaction();
    session.persist(shoppingCart);
    transaction.commit();
    session.close();
  }

  public List<ShoppingCart> listPart(Company company) {
    Session session = sessionFactory.openSession();
    String hql = "SELECT cs " +
        "FROM ShoppingCart cs " +
        "WHERE cs.company.name = :companyName ";
    Query query = session.createQuery(hql, ShoppingCart.class);
    query.setParameter("companyName", company.getName());
    List<ShoppingCart> shoppingCartList = query.list();
    return shoppingCartList;
  }
}
