package org.example.constants;

public class Message {
  public static final String ENTER_CHOICE = "\nPlease enter your choice from above options: ";
  public static final String PART_NAME = "Enter Part Name : ";
  public static final String MANUFACTURER_NAME = "Enter Manufacturer Name : ";
  public static final String PRICE = "Enter the Price : ";
  public static final String QUANTITY = "Enter the Quantity : ";
  public static final String AMOUNT = "Enter the Amount : ";
  public static final String INVALID_INPUT = "\nInvalid input. Please enter a number.";
  public static final String INVALID_INPUT_BOUND = "Invalid input. Please enter a number in range of provided above options";
  public static final String MAIN_MENU  = "\nMain Menu";
  public static final String MANUFACTURES_MENU  = "\nManufacturers Menu";
  public static final String SHOPPING_MENU  = "\nShopping Cart Menu";
  public static final String EXIT_MESSAGE  = "Exiting from the program...";
  public static final String PART_ADD_SUCCESSFULLY =  "Part added Successfully....";
  public static final String MANUFACTURER_ADD_SUCCESSFULLY =  "Manufacturer added successfully....";
  public static final String MANUFACTURER_REMOVED_SUCCESSFULLY =  "Manufacturer removed successfully....";
  public static final String BALANCE_ADD_SUCCESSFULLY =  "Balance added successfully....";
  public static final String PARTS_NOT_AVAILABLE = "Parts not available as according to requirement";
  public static final String MONEY_NOT_ENOUGH = "Not enough money to buy the parts";

}
