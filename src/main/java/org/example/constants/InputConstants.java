package org.example.constants;

public class InputConstants {

    public static final String DATA = "Data";

    public static final String MANUFACTURER = "Manufacturers";

    public static final String CART = "Shopping Cart";

    public static final String EXIT = "Exit";

    public static final String ADD_PART = "Add part";

    public static final String ADD_MANUFACTURER = "Add manufacturer";

    public static final String REMOVE_MANUFACTURER = "Remove manufacturer";

    public static final String ADD_QUANTITY = "Add quantity";

    public static final String LIST_QUANTITY = "List quantity";

    public static final String ADD_MONEY = "Add money";

    public static final String BUY_PARTS = "Buy parts";

    public static final String LIST_PARTS = "List parts";

    public static final String BACK = "Back";

}
