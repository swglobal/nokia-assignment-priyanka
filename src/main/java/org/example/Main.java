package org.example;

import org.example.menu.MainMenu;

public class Main {

  public static void main(String[] args) {
      loadMenu();
  }

  public static void loadMenu(){
    MainMenu mainMenu = new MainMenu();
    int choice = 0;
    do {
      mainMenu.display();
      choice = mainMenu.getUserChoice();
      mainMenu.handleUserChoice(choice);
    } while (choice != 4);
  }
}