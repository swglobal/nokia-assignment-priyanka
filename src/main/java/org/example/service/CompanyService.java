package org.example.service;

import org.example.constants.Message;
import org.example.model.Company;
import org.example.repository.CompanyRepository;

public class CompanyService {

  private final CompanyRepository companyRepository;

  public CompanyService(CompanyRepository companyRepository) {
    this.companyRepository = companyRepository;
  }

  public void addMoney(Double money) {

    try {
      Company company = companyRepository.getCompany();
      double newBalance = company.getBalance() + money;
      company.setBalance(newBalance);
      companyRepository.updateBalance(company);
      System.out.println(Message.BALANCE_ADD_SUCCESSFULLY);
      System.out.println("Updated balance: " + newBalance);
    } catch (Exception ex) {
        System.err.println("Could not update balance");
    }
  }
}
