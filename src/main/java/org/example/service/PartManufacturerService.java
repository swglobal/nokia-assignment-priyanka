package org.example.service;

import java.util.List;
import java.util.Optional;
import org.example.bussiness.PartManufacturerBusiness;
import org.example.model.Manufacturer;
import org.example.model.Part;
import org.example.model.PartManufacturer;
import org.example.repository.ManufacturerRepository;
import org.example.repository.PartRepository;
import org.example.repository.PartManufacturerRepository;

public class PartManufacturerService {

  private final PartManufacturerRepository partManufacturerRepository;
  private final PartManufacturerBusiness partManufacturerBusiness;
  private final ManufacturerRepository manufacturerRepository;
  private final PartRepository partRepository;

  public PartManufacturerService(PartManufacturerRepository partManufacturerRepository,
      PartManufacturerBusiness partManufacturerBusiness, ManufacturerRepository manufacturerRepository,
      PartRepository partRepository) {
    this.partManufacturerRepository = partManufacturerRepository;
    this.partManufacturerBusiness = partManufacturerBusiness;
    this.manufacturerRepository = manufacturerRepository;
    this.partRepository = partRepository;
  }


  public void addQuantity(String partName, String manufacturerName, int quantity, float price) {
    Optional<Part> part = getPart(partName);
    Optional<Manufacturer> manufacturer = getManufacturer(manufacturerName);

    if (part.isEmpty()) {
      String message = String.format("Part with Part Name '%s' does not exist", partName);
      System.err.println(message);
      return;
    }

    if (manufacturer.isEmpty()) {
      String message = String.format("Manufacturer with Manufacturer Name '%s' does not exist",
          manufacturerName);
      System.err.println(message);
      return;
    }

    PartManufacturer inputPartManufacturer = new PartManufacturer(null, part.get(),
        manufacturer.get(), quantity, price);
    PartManufacturer partManufacturer = partManufacturerBusiness.getPartManufacturer(
        inputPartManufacturer);
    try {
      partManufacturerRepository.savePartManufacturer(partManufacturer);
      System.out.println("\nQuantity updated successfully");
    } catch (Exception e) {
      String exceptionMessage = String.format("Part with Part Name '%s' does not exist", partName);
      System.err.println(exceptionMessage);
      e.printStackTrace();
    }
  }

  public List<PartManufacturer> listQuantity(String partName, String manufacturerName) {
    List<PartManufacturer> partManufacturerList = partManufacturerBusiness.getPartManufacturerList(
        partName, manufacturerName);
    return partManufacturerList;
  }

  public Optional<Part> getPart(String partName) {
    try {
      return partRepository.getByName(partName);
    } catch (Exception ex) {
      String exceptionMessage = String.format("Part with Name '%s' does not exist", partName);
      System.err.println(exceptionMessage);
      return Optional.empty();
    }
  }

  public Optional<Manufacturer> getManufacturer(String manufacturerName) {
    try {
      return manufacturerRepository.getByName(manufacturerName);
    } catch (Exception ex) {
      String exceptionMessage = String.format("Manufacturer with Name '%s' does not exist",
          manufacturerName);
      System.err.println(exceptionMessage);
      return Optional.empty();
    }
  }

}
