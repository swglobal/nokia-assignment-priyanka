package org.example.service;

import java.util.List;
import org.example.bussiness.ShoppingCartBusiness;
import org.example.bussiness.PartManufacturerBusiness;
import org.example.model.Company;
import org.example.model.ShoppingCart;
import org.example.model.PartManufacturer;
import org.example.repository.CompanyRepository;
import org.example.repository.ShoppingCartRepository;
import org.example.repository.PartManufacturerRepository;

public class ShoppingCartService {

  private final PartManufacturerRepository partManufacturerRepository;
  private final ShoppingCartRepository shoppingCartRepository;
  private final ShoppingCartBusiness shoppingCartBusiness;
  private final PartManufacturerBusiness partManufacturerBusiness;
  private final CompanyRepository companyRepository;

  public ShoppingCartService(PartManufacturerRepository partManufacturerRepository, CompanyRepository companyRepository,
      ShoppingCartRepository shoppingCartRepository, ShoppingCartBusiness shoppingCartBusiness,
      PartManufacturerBusiness partManufacturerBusiness) {
    this.partManufacturerRepository = partManufacturerRepository;
    this.companyRepository = companyRepository;
    this.partManufacturerBusiness = partManufacturerBusiness;
    this.shoppingCartRepository = shoppingCartRepository;
    this.shoppingCartBusiness = shoppingCartBusiness;
  }

  public void buyParts(String partName, int required_quantity) {
    Company company = companyRepository.getCompany();
    List<PartManufacturer> partManufacturerList = partManufacturerRepository.listQuantity(partName, "");
    List<PartManufacturer> boughtPartManufacturersList = shoppingCartBusiness.getBoughtPartManufacturers(
        partManufacturerList, company, required_quantity);

    if (boughtPartManufacturersList.size() != 0) {
      Double totalMoneySpent = 0.0;

      System.out.println();

      for (PartManufacturer boughtPartManufacturer : boughtPartManufacturersList) {
        totalMoneySpent += boughtPartManufacturer.getQuantity() * boughtPartManufacturer.getPrice();
        PartManufacturer updatedPartManufacturer = partManufacturerBusiness.getPartManufacturerWithUpdatedQuantiity(
            partManufacturerList, boughtPartManufacturer);

        ShoppingCart inputShoppingCart = new ShoppingCart(null, boughtPartManufacturer.getPart(),
            boughtPartManufacturer.getManufacturer(), company,
            boughtPartManufacturer.getQuantity());

        ShoppingCart shoppingCart = shoppingCartBusiness.getCompanyStock(inputShoppingCart);

        partManufacturerRepository.savePartManufacturer(updatedPartManufacturer);
        shoppingCartRepository.saveCompanyStock(shoppingCart);

        String message = String.format("%1$d quantity from company %2$s is added to the company",
            boughtPartManufacturer.getQuantity(),
            boughtPartManufacturer.getManufacturer().getName());
        System.out.println(message);
      }
      company.setBalance(company.getBalance() - totalMoneySpent);
      companyRepository.updateBalance(company);
    }
  }

  public List<ShoppingCart> listParts() {
    return shoppingCartRepository.listPart(companyRepository.getCompany());
  }
}
