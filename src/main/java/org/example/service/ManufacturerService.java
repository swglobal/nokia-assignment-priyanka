package org.example.service;

import org.example.bussiness.ManufacturerBusiness;
import org.example.constants.Message;
import org.example.model.Manufacturer;
import org.example.repository.ManufacturerRepository;
import org.example.repository.PartRepository;

public class ManufacturerService {

  private final ManufacturerRepository manufacturerRepository;
  private final ManufacturerBusiness manufacturerBusiness;
  private final PartRepository partRepository;

  public ManufacturerService(ManufacturerRepository manufacturerRepository,
      ManufacturerBusiness manufacturerBusiness,
      PartRepository partRepository) {
    this.manufacturerRepository = manufacturerRepository;
    this.manufacturerBusiness = manufacturerBusiness;
    this.partRepository = partRepository;
  }

  public void addManufacturer(String manufacturerName) {
    Manufacturer manufacturer = manufacturerBusiness.getManufacturer(manufacturerName);

    if (manufacturer.getId() == null) {
      manufacturerRepository.saveManufacturer(manufacturer);
      System.out.println(Message.MANUFACTURER_ADD_SUCCESSFULLY);
    } else {
      String message = String.format("Duplicate Manufacturer '%s' can't be added",
          manufacturerName);
      System.err.println(message);
    }
  }

  public void removeManufacturer(String manufacturerName) {
    Manufacturer manufacturer = manufacturerBusiness.getManufacturer(manufacturerName);

    if (manufacturer.getId() == null) {
      String message = String.format(
          "Manufacturer '%s' can't be deleted, as Manufacturer does not exists", manufacturerName);
      System.err.println(message);
    } else {
      manufacturerRepository.deleteManufacturer(manufacturer);
      System.err.println(Message.MANUFACTURER_REMOVED_SUCCESSFULLY);
    }
  }

}
