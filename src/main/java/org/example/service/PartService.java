package org.example.service;

import org.example.bussiness.PartBusiness;
import org.example.constants.Message;
import org.example.model.Part;
import org.example.repository.PartRepository;

public class PartService {

  private final PartRepository partRepository;
  private final PartBusiness partBusiness;

  public PartService(PartRepository partRepository, PartBusiness partBusiness) {
    this.partRepository = partRepository;
    this.partBusiness = partBusiness;
  }


  public void addPart(String partName) {
    Part part = partBusiness.getPart(partName);

    if (part.getId() == null) {
      partRepository.savePart(part);
      System.out.println(Message.PART_ADD_SUCCESSFULLY);
    } else {
      String exceptionMessage = String.format("Duplicate Part '%s' can't be added.....", partName);
      System.err.println(exceptionMessage);
    }
  }
}
