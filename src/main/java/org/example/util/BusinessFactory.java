package org.example.util;

import org.example.bussiness.ShoppingCartBusiness;
import org.example.bussiness.ManufacturerBusiness;
import org.example.bussiness.PartBusiness;
import org.example.bussiness.PartManufacturerBusiness;

public class BusinessFactory {

  private static ShoppingCartBusiness shoppingCartBusiness = null;

  private static ManufacturerBusiness manufacturerBusiness = null;

  private static PartBusiness partBusiness = null;

  private static PartManufacturerBusiness partManufacturerBusiness = null;

  public static ShoppingCartBusiness getCompanyStockBusiness() {
    if (shoppingCartBusiness == null) {
      shoppingCartBusiness = new ShoppingCartBusiness(DAOFactory.getCompanyStockDAO());
    }
    return shoppingCartBusiness;
  }

  public static ManufacturerBusiness getManufacturerBusiness() {
    if (manufacturerBusiness == null) {
      manufacturerBusiness = new ManufacturerBusiness(DAOFactory.getManufacturerDAO());
    }
    return manufacturerBusiness;
  }

  public static PartBusiness getPartBusiness() {
    if (partBusiness == null) {
      partBusiness = new PartBusiness(DAOFactory.getPartDAO());
    }
    return partBusiness;
  }

  public static PartManufacturerBusiness getPartManufacturerBusiness() {
    if (partManufacturerBusiness == null) {
      partManufacturerBusiness = new PartManufacturerBusiness(
          DAOFactory.getPartDAO(),
          DAOFactory.getManufacturerDAO(),
          DAOFactory.getPartManufacturerDAO());
    }
    return partManufacturerBusiness;
  }
}
