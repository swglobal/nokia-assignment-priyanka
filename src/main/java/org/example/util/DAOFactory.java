package org.example.util;

import org.example.repository.CompanyRepository;
import org.example.repository.ShoppingCartRepository;
import org.example.repository.ManufacturerRepository;
import org.example.repository.PartRepository;
import org.example.repository.PartManufacturerRepository;

public class DAOFactory {

  private static CompanyRepository companyRepository = null;

  private static ShoppingCartRepository shoppingCartRepository = null;

  private static ManufacturerRepository manufacturerRepository = null;

  private static PartRepository partRepository = null;

  private static PartManufacturerRepository partManufacturerRepository = null;

  public static CompanyRepository getCompanyDAO() {
    if (companyRepository == null) {
      companyRepository = new CompanyRepository();
    }
    return companyRepository;
  }

  public static ShoppingCartRepository getCompanyStockDAO() {
    if (shoppingCartRepository == null) {
      shoppingCartRepository = new ShoppingCartRepository();
    }
    return shoppingCartRepository;
  }

  public static ManufacturerRepository getManufacturerDAO() {
    if (manufacturerRepository == null) {
      manufacturerRepository = new ManufacturerRepository();
    }
    return manufacturerRepository;
  }

  public static PartRepository getPartDAO() {
    if (partRepository == null) {
      partRepository = new PartRepository();
    }
    return partRepository;
  }

  public static PartManufacturerRepository getPartManufacturerDAO() {
    if (partManufacturerRepository == null) {
      partManufacturerRepository = new PartManufacturerRepository();
    }
    return partManufacturerRepository;
  }

}
