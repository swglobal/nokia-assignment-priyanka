package org.example.util;

import org.example.service.CompanyService;
import org.example.service.ShoppingCartService;
import org.example.service.ManufacturerService;
import org.example.service.PartManufacturerService;
import org.example.service.PartService;

public class ServiceFactory {

  private static CompanyService companyService = null;

  private static ShoppingCartService shoppingCartService = null;

  private static ManufacturerService manufacturerService = null;

  private static PartService partService = null;

  private static PartManufacturerService partManufacturerService = null;

  public static CompanyService getCompanyService() {
    if (companyService == null) {
      companyService = new CompanyService(DAOFactory.getCompanyDAO());
    }
    return companyService;
  }

  public static ShoppingCartService getCompanyStockService() {
    if (shoppingCartService == null) {
      shoppingCartService = new ShoppingCartService(
          DAOFactory.getPartManufacturerDAO(),
          DAOFactory.getCompanyDAO(),
          DAOFactory.getCompanyStockDAO(),
          BusinessFactory.getCompanyStockBusiness(),
          BusinessFactory.getPartManufacturerBusiness());
    }
    return shoppingCartService;
  }

  public static ManufacturerService getManufacturerService() {
    if (manufacturerService == null) {
      manufacturerService = new ManufacturerService(
          DAOFactory.getManufacturerDAO(), BusinessFactory.getManufacturerBusiness(),
          DAOFactory.getPartDAO());
    }
    return manufacturerService;
  }

  public static PartService getPartService() {
    if (partService == null) {
      partService = new PartService(DAOFactory.getPartDAO(), BusinessFactory.getPartBusiness());
    }
    return partService;
  }

  public static PartManufacturerService getPartManufacturerService() {
    if (partManufacturerService == null) {
      partManufacturerService = new PartManufacturerService(DAOFactory.getPartManufacturerDAO(),
          BusinessFactory.getPartManufacturerBusiness(),
          DAOFactory.getManufacturerDAO(), DAOFactory.getPartDAO());
    }
    return partManufacturerService;
  }
}
