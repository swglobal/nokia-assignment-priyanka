## Assignment

The objective is to create a java cli project, which handles supply related task and pursuits of purchasing Parts from a diverse group of Manufacturers.
The application is contains Menu system, which facilitates navigation and execution of operations.


## DB setup

1. Download and install POSTGRESQL server from this link : https://www.postgresql.org/download/
2. Download and install PGAdmin : https://www.postgresql.org/download/
3. Configure the postgresql server in PGAdmin
4. Use following params
		-- Port 5432
		-- User - postgres
		-- Password - snippy066

5. Create database schema called "nokiadb"

#### Note :- connection of DB configuration in "hibernate.cfg.xml" , please refer that in case facing any problem


## Steps to run project

1. Setup jdk 17 in your system
2. Setup POSTGRESQL Database
2. Clone repository
3. Rebuild project
4. Run Main.java file


## Database Entity

We have defined 5 entities -

1. Company
2. Manufacturer
3. Part
4. PartManufacturer
5. ShoppingCart


## Project Architecture


### Menu Flow 
Maine menu extends an abstract class Menu which handles user choices. 
Once application is started MainMenu is displayed first with provided option and thus choosing an option leads to SubMenu.
SubMenu are child component of MainMenu . Once user made an appropriate choice it leads to operation Menu which handles service.


## Test Cases

There are two business class added for test :
PartBusinessTest
PartManufacturerBusinessTest

